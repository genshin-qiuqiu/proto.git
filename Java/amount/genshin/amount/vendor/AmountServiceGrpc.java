package genshin.amount.vendor;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.26.0)",
    comments = "Source: amount.proto")
public final class AmountServiceGrpc {

  private AmountServiceGrpc() {}

  public static final String SERVICE_NAME = "grpc.genshin.amount.AmountService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<genshin.amount.vendor.MessageSearch.HelloRequest,
      genshin.amount.vendor.MessageSearch.HelloResponse> getSayHelloMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "sayHello",
      requestType = genshin.amount.vendor.MessageSearch.HelloRequest.class,
      responseType = genshin.amount.vendor.MessageSearch.HelloResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<genshin.amount.vendor.MessageSearch.HelloRequest,
      genshin.amount.vendor.MessageSearch.HelloResponse> getSayHelloMethod() {
    io.grpc.MethodDescriptor<genshin.amount.vendor.MessageSearch.HelloRequest, genshin.amount.vendor.MessageSearch.HelloResponse> getSayHelloMethod;
    if ((getSayHelloMethod = AmountServiceGrpc.getSayHelloMethod) == null) {
      synchronized (AmountServiceGrpc.class) {
        if ((getSayHelloMethod = AmountServiceGrpc.getSayHelloMethod) == null) {
          AmountServiceGrpc.getSayHelloMethod = getSayHelloMethod =
              io.grpc.MethodDescriptor.<genshin.amount.vendor.MessageSearch.HelloRequest, genshin.amount.vendor.MessageSearch.HelloResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "sayHello"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  genshin.amount.vendor.MessageSearch.HelloRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  genshin.amount.vendor.MessageSearch.HelloResponse.getDefaultInstance()))
              .setSchemaDescriptor(new AmountServiceMethodDescriptorSupplier("sayHello"))
              .build();
        }
      }
    }
    return getSayHelloMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static AmountServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AmountServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AmountServiceStub>() {
        @java.lang.Override
        public AmountServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AmountServiceStub(channel, callOptions);
        }
      };
    return AmountServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static AmountServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AmountServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AmountServiceBlockingStub>() {
        @java.lang.Override
        public AmountServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AmountServiceBlockingStub(channel, callOptions);
        }
      };
    return AmountServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static AmountServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AmountServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AmountServiceFutureStub>() {
        @java.lang.Override
        public AmountServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AmountServiceFutureStub(channel, callOptions);
        }
      };
    return AmountServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class AmountServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void sayHello(genshin.amount.vendor.MessageSearch.HelloRequest request,
        io.grpc.stub.StreamObserver<genshin.amount.vendor.MessageSearch.HelloResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSayHelloMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSayHelloMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                genshin.amount.vendor.MessageSearch.HelloRequest,
                genshin.amount.vendor.MessageSearch.HelloResponse>(
                  this, METHODID_SAY_HELLO)))
          .build();
    }
  }

  /**
   */
  public static final class AmountServiceStub extends io.grpc.stub.AbstractAsyncStub<AmountServiceStub> {
    private AmountServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AmountServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AmountServiceStub(channel, callOptions);
    }

    /**
     */
    public void sayHello(genshin.amount.vendor.MessageSearch.HelloRequest request,
        io.grpc.stub.StreamObserver<genshin.amount.vendor.MessageSearch.HelloResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSayHelloMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class AmountServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<AmountServiceBlockingStub> {
    private AmountServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AmountServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AmountServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public genshin.amount.vendor.MessageSearch.HelloResponse sayHello(genshin.amount.vendor.MessageSearch.HelloRequest request) {
      return blockingUnaryCall(
          getChannel(), getSayHelloMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class AmountServiceFutureStub extends io.grpc.stub.AbstractFutureStub<AmountServiceFutureStub> {
    private AmountServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AmountServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AmountServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<genshin.amount.vendor.MessageSearch.HelloResponse> sayHello(
        genshin.amount.vendor.MessageSearch.HelloRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSayHelloMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SAY_HELLO = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AmountServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(AmountServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SAY_HELLO:
          serviceImpl.sayHello((genshin.amount.vendor.MessageSearch.HelloRequest) request,
              (io.grpc.stub.StreamObserver<genshin.amount.vendor.MessageSearch.HelloResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class AmountServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    AmountServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return genshin.amount.vendor.Amount.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("AmountService");
    }
  }

  private static final class AmountServiceFileDescriptorSupplier
      extends AmountServiceBaseDescriptorSupplier {
    AmountServiceFileDescriptorSupplier() {}
  }

  private static final class AmountServiceMethodDescriptorSupplier
      extends AmountServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    AmountServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (AmountServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new AmountServiceFileDescriptorSupplier())
              .addMethod(getSayHelloMethod())
              .build();
        }
      }
    }
    return result;
  }
}
