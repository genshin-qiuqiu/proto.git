// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: amount.proto

package genshin.amount.vendor;

public final class Amount {
  private Amount() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\014amount.proto\022\023grpc.genshin.amount\032\024mes" +
      "sage_search.proto2b\n\rAmountService\022Q\n\010sa" +
      "yHello\022!.grpc.genshin.amount.HelloReques" +
      "t\032\".grpc.genshin.amount.HelloResponseB\027\n" +
      "\025genshin.amount.vendorb\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          genshin.amount.vendor.MessageSearch.getDescriptor(),
        });
    genshin.amount.vendor.MessageSearch.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
