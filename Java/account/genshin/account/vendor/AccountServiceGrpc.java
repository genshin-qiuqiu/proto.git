package genshin.account.vendor;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.26.0)",
    comments = "Source: account.proto")
public final class AccountServiceGrpc {

  private AccountServiceGrpc() {}

  public static final String SERVICE_NAME = "grpc.genshin.account.AccountService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<genshin.account.vendor.MessageAccount.LoginReq,
      genshin.account.vendor.MessageAccount.LoginRsp> getLoginMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Login",
      requestType = genshin.account.vendor.MessageAccount.LoginReq.class,
      responseType = genshin.account.vendor.MessageAccount.LoginRsp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<genshin.account.vendor.MessageAccount.LoginReq,
      genshin.account.vendor.MessageAccount.LoginRsp> getLoginMethod() {
    io.grpc.MethodDescriptor<genshin.account.vendor.MessageAccount.LoginReq, genshin.account.vendor.MessageAccount.LoginRsp> getLoginMethod;
    if ((getLoginMethod = AccountServiceGrpc.getLoginMethod) == null) {
      synchronized (AccountServiceGrpc.class) {
        if ((getLoginMethod = AccountServiceGrpc.getLoginMethod) == null) {
          AccountServiceGrpc.getLoginMethod = getLoginMethod =
              io.grpc.MethodDescriptor.<genshin.account.vendor.MessageAccount.LoginReq, genshin.account.vendor.MessageAccount.LoginRsp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Login"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  genshin.account.vendor.MessageAccount.LoginReq.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  genshin.account.vendor.MessageAccount.LoginRsp.getDefaultInstance()))
              .setSchemaDescriptor(new AccountServiceMethodDescriptorSupplier("Login"))
              .build();
        }
      }
    }
    return getLoginMethod;
  }

  private static volatile io.grpc.MethodDescriptor<genshin.account.vendor.MessageAccount.LogoutReq,
      genshin.account.vendor.MessageAccount.LogoutRsp> getLogoutMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Logout",
      requestType = genshin.account.vendor.MessageAccount.LogoutReq.class,
      responseType = genshin.account.vendor.MessageAccount.LogoutRsp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<genshin.account.vendor.MessageAccount.LogoutReq,
      genshin.account.vendor.MessageAccount.LogoutRsp> getLogoutMethod() {
    io.grpc.MethodDescriptor<genshin.account.vendor.MessageAccount.LogoutReq, genshin.account.vendor.MessageAccount.LogoutRsp> getLogoutMethod;
    if ((getLogoutMethod = AccountServiceGrpc.getLogoutMethod) == null) {
      synchronized (AccountServiceGrpc.class) {
        if ((getLogoutMethod = AccountServiceGrpc.getLogoutMethod) == null) {
          AccountServiceGrpc.getLogoutMethod = getLogoutMethod =
              io.grpc.MethodDescriptor.<genshin.account.vendor.MessageAccount.LogoutReq, genshin.account.vendor.MessageAccount.LogoutRsp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Logout"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  genshin.account.vendor.MessageAccount.LogoutReq.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  genshin.account.vendor.MessageAccount.LogoutRsp.getDefaultInstance()))
              .setSchemaDescriptor(new AccountServiceMethodDescriptorSupplier("Logout"))
              .build();
        }
      }
    }
    return getLogoutMethod;
  }

  private static volatile io.grpc.MethodDescriptor<genshin.account.vendor.MessageAccount.RegistryReq,
      genshin.account.vendor.MessageAccount.RegistryRsp> getRegistryMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Registry",
      requestType = genshin.account.vendor.MessageAccount.RegistryReq.class,
      responseType = genshin.account.vendor.MessageAccount.RegistryRsp.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<genshin.account.vendor.MessageAccount.RegistryReq,
      genshin.account.vendor.MessageAccount.RegistryRsp> getRegistryMethod() {
    io.grpc.MethodDescriptor<genshin.account.vendor.MessageAccount.RegistryReq, genshin.account.vendor.MessageAccount.RegistryRsp> getRegistryMethod;
    if ((getRegistryMethod = AccountServiceGrpc.getRegistryMethod) == null) {
      synchronized (AccountServiceGrpc.class) {
        if ((getRegistryMethod = AccountServiceGrpc.getRegistryMethod) == null) {
          AccountServiceGrpc.getRegistryMethod = getRegistryMethod =
              io.grpc.MethodDescriptor.<genshin.account.vendor.MessageAccount.RegistryReq, genshin.account.vendor.MessageAccount.RegistryRsp>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Registry"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  genshin.account.vendor.MessageAccount.RegistryReq.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  genshin.account.vendor.MessageAccount.RegistryRsp.getDefaultInstance()))
              .setSchemaDescriptor(new AccountServiceMethodDescriptorSupplier("Registry"))
              .build();
        }
      }
    }
    return getRegistryMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static AccountServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AccountServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AccountServiceStub>() {
        @java.lang.Override
        public AccountServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AccountServiceStub(channel, callOptions);
        }
      };
    return AccountServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static AccountServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AccountServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AccountServiceBlockingStub>() {
        @java.lang.Override
        public AccountServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AccountServiceBlockingStub(channel, callOptions);
        }
      };
    return AccountServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static AccountServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AccountServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AccountServiceFutureStub>() {
        @java.lang.Override
        public AccountServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AccountServiceFutureStub(channel, callOptions);
        }
      };
    return AccountServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class AccountServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * 登录
     * </pre>
     */
    public void login(genshin.account.vendor.MessageAccount.LoginReq request,
        io.grpc.stub.StreamObserver<genshin.account.vendor.MessageAccount.LoginRsp> responseObserver) {
      asyncUnimplementedUnaryCall(getLoginMethod(), responseObserver);
    }

    /**
     * <pre>
     * 登出
     * </pre>
     */
    public void logout(genshin.account.vendor.MessageAccount.LogoutReq request,
        io.grpc.stub.StreamObserver<genshin.account.vendor.MessageAccount.LogoutRsp> responseObserver) {
      asyncUnimplementedUnaryCall(getLogoutMethod(), responseObserver);
    }

    /**
     * <pre>
     * 注册
     * </pre>
     */
    public void registry(genshin.account.vendor.MessageAccount.RegistryReq request,
        io.grpc.stub.StreamObserver<genshin.account.vendor.MessageAccount.RegistryRsp> responseObserver) {
      asyncUnimplementedUnaryCall(getRegistryMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getLoginMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                genshin.account.vendor.MessageAccount.LoginReq,
                genshin.account.vendor.MessageAccount.LoginRsp>(
                  this, METHODID_LOGIN)))
          .addMethod(
            getLogoutMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                genshin.account.vendor.MessageAccount.LogoutReq,
                genshin.account.vendor.MessageAccount.LogoutRsp>(
                  this, METHODID_LOGOUT)))
          .addMethod(
            getRegistryMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                genshin.account.vendor.MessageAccount.RegistryReq,
                genshin.account.vendor.MessageAccount.RegistryRsp>(
                  this, METHODID_REGISTRY)))
          .build();
    }
  }

  /**
   */
  public static final class AccountServiceStub extends io.grpc.stub.AbstractAsyncStub<AccountServiceStub> {
    private AccountServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AccountServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AccountServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     * 登录
     * </pre>
     */
    public void login(genshin.account.vendor.MessageAccount.LoginReq request,
        io.grpc.stub.StreamObserver<genshin.account.vendor.MessageAccount.LoginRsp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 登出
     * </pre>
     */
    public void logout(genshin.account.vendor.MessageAccount.LogoutReq request,
        io.grpc.stub.StreamObserver<genshin.account.vendor.MessageAccount.LogoutRsp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 注册
     * </pre>
     */
    public void registry(genshin.account.vendor.MessageAccount.RegistryReq request,
        io.grpc.stub.StreamObserver<genshin.account.vendor.MessageAccount.RegistryRsp> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getRegistryMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class AccountServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<AccountServiceBlockingStub> {
    private AccountServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AccountServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AccountServiceBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * 登录
     * </pre>
     */
    public genshin.account.vendor.MessageAccount.LoginRsp login(genshin.account.vendor.MessageAccount.LoginReq request) {
      return blockingUnaryCall(
          getChannel(), getLoginMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 登出
     * </pre>
     */
    public genshin.account.vendor.MessageAccount.LogoutRsp logout(genshin.account.vendor.MessageAccount.LogoutReq request) {
      return blockingUnaryCall(
          getChannel(), getLogoutMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 注册
     * </pre>
     */
    public genshin.account.vendor.MessageAccount.RegistryRsp registry(genshin.account.vendor.MessageAccount.RegistryReq request) {
      return blockingUnaryCall(
          getChannel(), getRegistryMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class AccountServiceFutureStub extends io.grpc.stub.AbstractFutureStub<AccountServiceFutureStub> {
    private AccountServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AccountServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AccountServiceFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * 登录
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<genshin.account.vendor.MessageAccount.LoginRsp> login(
        genshin.account.vendor.MessageAccount.LoginReq request) {
      return futureUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 登出
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<genshin.account.vendor.MessageAccount.LogoutRsp> logout(
        genshin.account.vendor.MessageAccount.LogoutReq request) {
      return futureUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 注册
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<genshin.account.vendor.MessageAccount.RegistryRsp> registry(
        genshin.account.vendor.MessageAccount.RegistryReq request) {
      return futureUnaryCall(
          getChannel().newCall(getRegistryMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_LOGIN = 0;
  private static final int METHODID_LOGOUT = 1;
  private static final int METHODID_REGISTRY = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AccountServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(AccountServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_LOGIN:
          serviceImpl.login((genshin.account.vendor.MessageAccount.LoginReq) request,
              (io.grpc.stub.StreamObserver<genshin.account.vendor.MessageAccount.LoginRsp>) responseObserver);
          break;
        case METHODID_LOGOUT:
          serviceImpl.logout((genshin.account.vendor.MessageAccount.LogoutReq) request,
              (io.grpc.stub.StreamObserver<genshin.account.vendor.MessageAccount.LogoutRsp>) responseObserver);
          break;
        case METHODID_REGISTRY:
          serviceImpl.registry((genshin.account.vendor.MessageAccount.RegistryReq) request,
              (io.grpc.stub.StreamObserver<genshin.account.vendor.MessageAccount.RegistryRsp>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class AccountServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    AccountServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return genshin.account.vendor.Account.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("AccountService");
    }
  }

  private static final class AccountServiceFileDescriptorSupplier
      extends AccountServiceBaseDescriptorSupplier {
    AccountServiceFileDescriptorSupplier() {}
  }

  private static final class AccountServiceMethodDescriptorSupplier
      extends AccountServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    AccountServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (AccountServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new AccountServiceFileDescriptorSupplier())
              .addMethod(getLoginMethod())
              .addMethod(getLogoutMethod())
              .addMethod(getRegistryMethod())
              .build();
        }
      }
    }
    return result;
  }
}
