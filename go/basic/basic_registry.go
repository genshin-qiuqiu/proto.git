package basic

import (
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/http"
)

func BasicRegistry(basicServer BasicServer) {
	e := &Global{}
	e.Registry(basicServer)
}

type Global struct {
	client BasicClient
}

func (g *Global) GG(err error) {
	if err != nil {
		panic(err)
	}
}
func (g *Global) Warning(err error) {
	if err != nil {
		log.Println(err)
	}
}

func (g *Global) BasicServices(basicServer BasicServer) {
	s := grpc.NewServer()
	RegisterBasicServer(s, basicServer)
	listener, err := net.Listen("tcp", ":28801")
	g.GG(err)
	g.GG(s.Serve(listener))
}

func (g *Global) Registry(basicServer BasicServer) {

	go g.BasicServices(basicServer)

	conn, err := grpc.Dial("localhost:28801", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	g.client = NewBasicClient(conn)

	web := gin.Default()
	web.POST("/GetBackGroundImageUrl", g.GetBackGroundImageUrl)
	g.GG(web.Run(":18801"))
}

func (g *Global) GetBackGroundImageUrl(ctx *gin.Context) {

	req := GetBackGroundImageUrlReq{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := g.client.GetBackGroundImageUrl(ctx, &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, res)
}
